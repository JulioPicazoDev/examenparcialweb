package com.softtek.academy.javaweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.softtek.academy.javaweb.model.ConexDB;

/**
 * UpdateToDoDao
 */
public class UpdActDAO {

    public Boolean updActs(int _idAct, int _Isdone) {
        try {
        	Connection conex = ConexDB.getInstance().getConnection();
            PreparedStatement ps = conex.prepareStatement("update TO_DO_LIST set is_done = ? where id = ?");
            ps.setInt(1, _Isdone);
            ps.setInt(2, _idAct);
            
            int i = ps.executeUpdate();
            
            return i > 0 ? true : false;
            
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}