package com.softtek.academy.javaweb.model;

public class ActivityBean {
	private int id;
	private String list;
	private int isDone;
	
	public ActivityBean(int id, String list, int done) {
		this.setId(id);
		this.setList(list);
		this.setIsDone(done);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getList() {
		return list;
	}

	public void setList(String list) {
		this.list = list;
	}

	public int getIsDone() {
		return isDone;
	}

	public void setIsDone(int isDone) {
		this.isDone = isDone;
	}
	

	
}
