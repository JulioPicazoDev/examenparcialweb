<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!DOCTYPE html>
<html>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<head>
<meta charset="UTF-8">
    <title>Activities</title>
</head>

<body>
<br>
    <table class="table">
    	<thead class="thead-dark">
        <th>Id of activity</th>
        <th>Name activity</th>
        <th>Is done?</th>
         </thead>
            <c:forEach items="${list}" var="act">
                <tr>
                    <td>
                    ${act.id}
                    </td>

                   <td>${act.list}</td>
                
                
                <c:if test="${act.isDone == 0}">
                
                <td><a href="UpdateList?id=${act.id}&done=1">Done</a></td>
                </tr>
            	</c:if>
                <c:if test="${act.isDone == 1}">

                <td><a href="UpdateList?id=${act.id}&done=0">Is Not Done</a></td>
                </tr>
            	</c:if>
            </c:forEach>

    </table>
</body>

</html>